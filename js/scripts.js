// scroll plugin
(function($) {
    $.fn.goTo = function() {
        $('html, body').animate({
            scrollTop: $(this).offset().top + 'px'
        }, 600);
        return this;
    }
})(jQuery);

// constants
var yearSpanDefault = 5;
var animateDuration=600;
var w = 970;
var padding1 =40;
var padding2 = 90;
var padding
var h = 650;

// shared variables
var currentStartInd;
var currentEndInd;
var currentData;
var lineStartData;
var lineEndData; 
var lineData;
var xscale;

// chart objects
var pie;
var map;

// color scheme
var color={
  Climatological:{
    defaultFill:'#ffffb2',
    "0~3 cases":'#fecc5c',
    "3~5 cases":'#fd8d3c',
    ">5 cases":'#e31a1c'
  },
  Geophysical:{
    defaultFill:'#edf8fb',
    "0~3 cases":'#b3cde3',
    "3~5 cases":'#8c96c6',
    ">5 cases":'#88419d',
  },
  Hydrological:{
    defaultFill:'#f0f9e8',
    "0~3 cases":'#bae4bc',
    "3~5 cases":'#7bccc4',
    ">5 cases":'#2b8cbe'
  },
  default_color:{
    defaultFill:'#555'
  }
};

// number format helper
function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

// init and update functions
// init all charts at the very beginning
function init(){
  d3.json("data/dataset.json", function(error, dataset){
    d3.select("#line_name").append("text").text("The Economic and Human Impact of Disasters");
    var svg = d3.select("#lines").append("svg").attr({id:"line", width:w, height:h });

    drawLine(dataset,"Death",svg);
    drawLine(dataset,"Affected",svg);
    drawLine(dataset,"Damage",svg);
    d3.select("#next")
        .on("click",function(){
          onNextClick(dataset);
        });
    d3.select("#prev")
        .on("click",function(){
          onPrevClick(dataset);
        });
    var year = dataset[0].Year;
    for (var i in dataset){
      if (dataset[i].Year == year){
        var Geo = dataset[i].Geophysical;
        var Cli = dataset[i].Climatological;
        var Hyd = dataset[i].Hydrological;
      }
    }  
    d3.select("#pie_name").append("text").text(['World Disasters Category Composition in',year].join(' '))
    bakePie(Geo,Cli,Hyd,year);
    initChoropleth();
    d3.json(['data/choroplethdata/','Hydrological','/',year,'.json'].join(''), function(error, dataset){
      // console.log(geophysic_color)
      updateChoropleth(dataset,'Hydrological',''+year);
    });
  });
}
// init line chart
function drawLine(dataset,lineName,holder){

  var max = d3.max(dataset,function(d){return d[lineName]});
  var min = d3.min(dataset,function(d){return d[lineName]});
  var svg = holder;

  currentStartInd = 0;
  currentEndInd = currentStartInd+yearSpanDefault;
  currentData = dataset.slice(currentStartInd,currentEndInd+1);
  lineStartData = dataset.slice(currentStartInd,currentEndInd);
  lineEndData = dataset.slice(currentStartInd+1,currentEndInd+1); 
  lineData = lineStartData.map(function(d,i){
        return {
          key:currentStartInd+i,
          year1:lineStartData[i].Year,
          year2:lineEndData[i].Year,
          value1:lineStartData[i][lineName],
          value2:lineEndData[i][lineName]
        }
    });
  xscale = d3.scale.ordinal()
                .domain(currentData.map(function(d){ return d.Year; }))
                .rangeRoundPoints([padding2,w-padding2],0.2);
  
  var h_mid = h-padding2*2-padding1;
  var top = padding1+padding2;
  if (lineName=="Death"){
    var yscale = d3.scale.linear()
                .domain([min,max])
                .range([top+h_mid/3,top]);
    var color = "#f4b24c"
    var color_h = "#ffeda0"
  }
  else if (lineName == "Affected"){
    var yscale = d3.scale.linear()
                .domain([min,max])
                .range([top+h_mid/3*2,top+h_mid/3+padding1]);
    var color = "#86c166"
    var color_h = "#a8ddb5"
  }
  else{
    var yscale = d3.scale.linear()
                .domain([min,max])
                .range([h-padding2,top+h_mid/3*2+padding1]);
    var color = "#9ecae1"
    var color_h = "#deebf7"
  } 
  var gcontainer = svg.append("g").attr("id",lineName);

  gcontainer.selectAll("line")
      .data(lineData)
      .enter()
      .append("line")
      .attr({
        x1:function(d,i){ return xscale(d.year1); },
        y1:function(d,i){ return yscale(d.value1); },
        x2:function(d,i){ return xscale(d.year2); },
        y2:function(d,i){ return yscale(d.value2); },
        stroke: color
      })
      .style("stroke-width",4);


  gcontainer.selectAll(".circle")
      .data(currentData)
      .enter()
      .append("circle")
      .attr({
        class:"circle",
        cx:function(d,i){ return xscale(d.Year); },
        cy:function(d,i){ return yscale(d[lineName]); },
        r:10
      })
      .style("fill",color)
      .on("mouseover",function(d){
        d3.select(this).style("fill",color_h);
        var xPosition = parseFloat(d3.select(this).attr('cx'))  + 20;
        var yPosition = parseFloat(d3.select(this).attr('cy')) + 120;
            // console.log(xPosition+' '+yPosition);
        if(lineName=="Death"){
        var tooltip_html_str = ['<p>In',d.Year+',','</p>','<p>Total Death: <b>',numberWithCommas(d[lineName]),'</b> people </p>'].join(' ');
        }else if(lineName=="Affected"){
        var tooltip_html_str = ['<p>In',d.Year+',',',</p>','<p>Total Affected: <b>',numberWithCommas(d[lineName])+'k','</b> people </p>'].join(' ');
        }else{
        var tooltip_html_str = ['<p>In',d.Year+',',',</p>','<p>Total Damage: <b>$',numberWithCommas(d[lineName])+'k','</b></p>'].join(' ');
        }
        d3.select('#tooltip')
                .style('left', xPosition + 'px')
                .style('top', yPosition + 'px')
                .select('#info')
                .html(tooltip_html_str);
        d3.select('#tooltip').classed('hidden', false);
      })
      .on("mouseout",function(d){
        d3.select(this).style("fill",color);
        d3.select('#tooltip').classed('hidden', true);
      })
      .on("click",function(d){
        year = d.Year;
        // console.log(year);too
        for (var i in dataset){
          if (dataset[i].Year == year){
            var Geo = dataset[i].Geophysical;
            var Cli = dataset[i].Climatological;
            var Hyd = dataset[i].Hydrological;
          }
        }
        d3.select("#pieChart").html('');
        bakePie(Geo,Cli,Hyd,year);
        $("#pieholder").goTo();
      });
  gcontainer.selectAll(".label")
      .data(currentData)
      .enter()
      .append("text")
      .attr({
        class:"label",
        x:function(d,i){ return xscale(d.Year); },
        y:function(d,i){ return yscale(d[lineName])-15; }
      })
      .style("fill",color)
      .text(function(d){ 
        if (lineName == "Death") {return numberWithCommas(d[lineName]);} 
        else{return numberWithCommas(d[lineName])+'k';};
         });
  
  if (lineName=="Damage"){
        var xaxis = d3.svg.axis()
              .scale(xscale)
              .orient("bottom")
              .tickFormat(d3.format("d"));
    gcontainer.append("g")
      .attr({
        class:"axis x",
        transform:"translate(0,"+(h-padding2/2)+")"
      })
      .call(xaxis);

  } else if (lineName=="Death"){
        var xaxis = d3.svg.axis()
              .scale(xscale)
              .orient("top")
              .tickFormat(d3.format("d"));
    gcontainer.append("g")
      .attr({
        class:"axis x",
        transform:"translate(0,"+(padding2)+")"
      })
      .call(xaxis);
  }

  gcontainer.append("circle").attr({cx:155,cy:30,r:10,fill:"#f4b24c"});
  gcontainer.append("circle").attr({cx:350,cy:30,r:10,fill:"#86c166"});
  gcontainer.append("circle").attr({cx:530,cy:30,r:10,fill:"#9ecae1"});

  gcontainer.append("text").attr({x:15,y:35,fill:"#f4b24c"}).attr("font-size","18px").text("Death (People)");
  gcontainer.append("text").attr({x:190,y:35,fill:"#86c166"}).attr("font-size","18px").text("Affected (People)");
  gcontainer.append("text").attr({x:385,y:35,fill:"#9ecae1"}).attr("font-size","18px").text("Damage (USD)");

 }
// update line chart
function updateLine(dataset,tempStartInd,tempEndInd,lineName,direction){
  var max = d3.max(dataset,function(d){return d[lineName]});
  var min = d3.min(dataset,function(d){return d[lineName]});
  var h_mid = h-padding2*2-padding1;
  var top = padding1+padding2;              
  if (lineName=="Death"){
    var yscale = d3.scale.linear()
                .domain([min,max])
                .range([top+h_mid/3,top]);
    var color = "#f4b24c"
    var color_h = "#ffeda0"
  }
  else if (lineName == "Affected"){
    var yscale = d3.scale.linear()
                .domain([min,max])
                .range([top+h_mid/3*2,top+h_mid/3+padding1]);
    var color = "#86c166"
    var color_h = "#a8ddb5"
  }
  else{
    var yscale = d3.scale.linear()
                .domain([min,max])
                .range([h-padding2,top+h_mid/3*2+padding1]);
    var color = "#9ecae1"
    var color_h = "#deebf7"
  } 

  var step=1;
  var delayDuration=ind/yearSpanDefault*animateDuration;
  tempStartInd = tempStartInd+step*direction;
  tempEndInd = tempStartInd+yearSpanDefault;
  var tempData = dataset.slice(tempStartInd,tempEndInd+1);
  var templineStartData = dataset.slice(tempStartInd,tempEndInd);
  var templineEndData = dataset.slice(tempStartInd+1,tempEndInd+1);
  var templineData = templineStartData.map(function(d,i){
            return {
              key:tempStartInd+i,
              year1:templineStartData[i].Year,
              year2:templineEndData[i].Year,
              value1:templineStartData[i][lineName],
              value2:templineEndData[i][lineName]
            }
          });

  var xscale = d3.scale.ordinal()
                .domain(tempData.map(function(d){ return d.Year; }))
                .rangeRoundPoints([padding2,w-padding2],0.2);;
  d3.select(['#',lineName].join('')).selectAll("line")
            .data(templineData)
            .transition()
            .delay(delayDuration)
            .duration(animateDuration/yearSpanDefault)
            .attr({
              x1:function(d,i){ return xscale(d.year1); },
              y1:function(d,i){ return yscale(d.value1); },
              x2:function(d,i){ return xscale(d.year2); },
              y2:function(d,i){ return yscale(d.value2); },
              stroke:color
            });
  d3.select(['#',lineName].join('')).selectAll(".circle")
            .data(tempData)
            .transition()
            .delay(delayDuration)
            .duration(animateDuration/yearSpanDefault)
            .attr({
              cx:function(d,i){ return xscale(d.Year); },
              cy:function(d,i){ return yscale(d[lineName]); },
            })
            //.style("fill",color)
            ;
  d3.select(['#',lineName].join('')).selectAll(".label")
            .data(tempData)
            .transition()
            .delay(delayDuration)
            .duration(animateDuration/yearSpanDefault)
            .attr({
              x:function(d,i){ return xscale(d.Year); },
              y:function(d,i){ return yscale(d[lineName])-15; }
            })
            .text(function(d){ 
        if (lineName == "Death") {return numberWithCommas(d[lineName]);} 
        else{return numberWithCommas(d[lineName])+'k';};
         });
  if (lineName=="Damage"){
    var xaxis = d3.svg.axis()
            .scale(xscale)
            .orient("bottom")
            .tickFormat(d3.format("d"));
    d3.select(['#',lineName].join('')).select(".x")
            .transition()
            .delay(delayDuration)
            .duration(animateDuration/yearSpanDefault)
            .call(xaxis);
  }
  else if (lineName=="Death"){
    var xaxis = d3.svg.axis()
            .scale(xscale)
            .orient("top")
            .tickFormat(d3.format("d"));
    d3.select(['#',lineName].join('')).select(".x")
            .transition()
            .delay(delayDuration)
            .duration(animateDuration/yearSpanDefault)
            .call(xaxis);
  }
}
// init or update pie chart
function bakePie(Geo,Cli,Hyd,year){
  d3.select("#pie_name").select("text").text(['World Disasters Category Composition in',year].join(' '))

  pie = new d3pie("pieChart", {
          "size": {
            "canvasWidth": 1000
          },
          "data": {
            "sortOrder": "value-desc",
            "content": [
              {
                "label": "Geophysical",
                "value": Geo,
                "color": "#88419d "
              },
              {
                "label": "Climatological",
                "value": Cli,
                "color": "#eb7e0e"
              },
              {
                "label": "Hydrological",
                "value": Hyd,
                "color": "#14b0da"
              }
            ]
          },
          "labels": {
            "outer": {
              "pieDistance": 32
            },
            "mainLabel": {
              "fontSize": 32,
              "color":"rgba(255,255,255,.75)"
            },
            "percentage": {
              "color": "#fff",
              "decimalPlaces": 0,
              "fontSize":15
            },
            "value": {
              "color": "#adadad",
              "fontSize": 11
            },
            "lines": {
              "enabled": true
            }
          },
          "tooltips": {
            "enabled": true,
            "type": "placeholder",
            "string": "{label}: Count:{value} Cases",
            "styles": {
              "backgroundColor": "#555",
              "backgroundOpacity": 1
            }
          },
          "affects": {
            "pullOutSegmentOnClick": {
              "affect": "linear",
              "speed": 400,
              "size": 8
            }
          },
          "misc": {
            "gradient": {
              "enabled": true,
              "percentage": 100,
              "color": "#726060"
            }
          },
          "callbacks": {
            onClickSegment: function(a) {
              onCategoryClick(a.data.label,year);
              $("#choroplethholder").goTo();
            }
          }
        });
}
// init choropleth
function initChoropleth(){
  d3.select("#map_name").append("text").text("World Disasters Choropleth Map")
  map = new Datamap({
    element: document.getElementById('choropleth'),
    geographyConfig: {
      popupOnHover: false,
      highlightOnHover: false,
      borderColor:'#555'
    },
    fills: color.default_color,
    data:{}
  });
}
// update choropleth map
function updateChoropleth(dataset,category,year){
  d3.select("#choropleth").html('');
  d3.select("#map_name").select("text").text(['World',category,'Disasters','Choropleth Map','in',year].join(' '))

  map = new Datamap({
    element: document.getElementById('choropleth'),
    fills:color[category],
    data:dataset,
    geographyConfig: {
      popupTemplate: function(geo, data) {
        if (data==null)
          return ['<div class="hoverinfo" style="background-color:#555"><strong>',
                'No report of ' + category + ' disasters in ' + geo.properties.name,
                '</strong></div>'].join('');
        else
          return ['<div class="hoverinfo" style="background-color:#555"><strong>',
                'Number of '+ category +' disasters in ' + geo.properties.name,
                ': ' + data.number,
                '</strong></div>'].join('');
      }
    },
    done: function(datamap) {
      datamap.svg.selectAll('.datamaps-subunit').on('click', function(geography) {
          // console.log(geography.id);
          onCountryClick(geography.id,category,year);
      });
    }
  });
  d3.select("#choropleth").select("svg").append("g");
  map.legend();
}

// event handlers
// handle line chart 'prev' button click
function onPrevClick(dataset){ // update line
      if (currentEndInd+yearSpanDefault+1<=dataset.length) {
        console.log("valid prev click");
        for (ind=0;ind<yearSpanDefault;ind++){
          var tempstartind = currentStartInd;
          var tempendind = currentEndInd;
          updateLine(dataset,tempstartind,tempendind,"Death",1);
          updateLine(dataset,tempstartind,tempendind,"Affected",1);
          updateLine(dataset,tempstartind,tempendind,"Damage",1); 
          currentStartInd=tempstartind+1;
          currentEndInd=tempendind+1;
        }
        d3.select("#prev")
            .classed("disabled",(currentEndInd+yearSpanDefault+1>dataset.length));
        d3.select("#next")
            .classed("disabled",(currentStartInd-yearSpanDefault<0));
        $("#linesholder").goTo();
      } else {
        console.log("prev reached end");
      }
}
// handle line chart 'next' button click
function onNextClick(dataset){ // update line
  if (currentStartInd-yearSpanDefault>=0) {
        console.log("valid next click");
        for (ind=0;ind<yearSpanDefault;ind++){
          var tempstartind = currentStartInd;
          var tempendind = currentEndInd;
          updateLine(dataset,tempstartind,tempendind,"Death",-1);
          updateLine(dataset,tempstartind,tempendind,"Affected",-1);
          updateLine(dataset,tempstartind,tempendind,"Damage",-1); 
          currentStartInd=tempstartind-1;
          currentEndInd=tempendind-1;
        }

        d3.select("#prev")
            .classed("disabled",(currentEndInd+yearSpanDefault+1>dataset.length));
        d3.select("#next")
            .classed("disabled",(currentStartInd-yearSpanDefault<0)); 
      } else {
        console.log("next reached end");
      }
}
// handle pie chart click
function onCategoryClick(category,year){
  console.log(['data/choroplethdata/',category,'/',year,'.json'].join(''));
  d3.json(['data/choroplethdata/',category,'/',year,'.json'].join(''), function(error, dataset){
    // console.log(geophysic_color)
    updateChoropleth(dataset,category,''+year);
  });
}
// handle choropleth click
function onCountryClick(country,category,year){
  console.log(country);
  console.log(['data/tabledata/',category,'/',country,'.json'].join(''))
  d3.json(['data/tabledata/',category,'/',country,'.json'].join(''), function(error, dataset){
    if (error){
      console.log("catch");
      d3.select("#table").html('')
      .append("thead").html(
        ['<tr>',
        '<th>Year</th>',
        '<th>Country</th>',
        '<th>Type</th>',
        '<th>Sub Type</th>',
        '<th>Number of Death</th>',
        '<th>Number of Affected</th>',
        '<th>Financial Damage</th>',
        '</tr>'].join('')
      );
    } else {
      d3.select("#table").html('')
      .append("thead").html(
        ['<tr>',
        '<th>Year</th>',
        '<th>Country</th>',
        '<th>Type</th>',
        '<th>Sub Type</th>',
        '<th>Number of Death</th>',
        '<th>Number of Affected</th>',
        '<th>Financial Damage</th>',
        '</tr>'].join('')
      );
      var columns=Object.keys(dataset[0]);
      var subset = dataset.filter(function(obj){return (obj["year"]==year);});
      console.log(subset);
      d3.select("#table").append("tbody")
        .selectAll("tr")
        .data(subset)
        .enter()
        .append("tr")
        .selectAll("td")
        .data(function(row){//transform the data bound to each "tr" element to {colum: col_name, value: val}
          return columns.map(function(column) { 
            return {column: column, value: row[column]}; 
          });
        })
        .enter()
        .append("td")
        .text(function(datum){
          return datum.value;
        });
        $("#table").goTo();
    }
  });
}
// handle 'show me how' button click
function startIntro(){
    var intro = introJs();
      intro.setOptions({
        steps: [
          { 
            intro: "This visualization works in a <b>Drill Down</b> flavor!"
          },
          { 
            intro: "In the line charts, you can select a data point to specify the attribute <i>Year</i>"
          },
          {
            element: document.querySelector('#pieChart'),
            intro: "Select a slice to determine <i>Category</i> to be shown in the Choropleth Map"
          },
          {
            element: document.querySelectorAll('#choropleth')[0],
            intro: "Pick a <i>Country</i> to finalize!",
          }
        ]
      });

      intro.start();
}

$("#contact").hide();
$("#about").hide();
init();

$( "ul li:nth-child(2)")
.on("click",function(){
    $("#present").hide();
    $("#contact").hide();
    $("#about").show();
    $( "ul li:nth-child(2)").addClass("active");
    $( "ul li:nth-child(1)").removeClass("active");
    $( "ul li:nth-child(3)").removeClass("active");
})

$( "ul li:nth-child(1)")
.on("click",function(){
    $("#present").show();
    $("#about").hide();
    $("#contact").hide();
    $( "ul li:nth-child(1)").addClass("active");
    $( "ul li:nth-child(2)").removeClass("active");
    $( "ul li:nth-child(3)").removeClass("active");  
})

$( "ul li:nth-child(3)")
.on("click",function(){
    $("#present").hide();
    $("#about").hide();
    $("#contact").show();
    $( "ul li:nth-child(3)").addClass("active");
    $( "ul li:nth-child(1)").removeClass("active");
    $( "ul li:nth-child(2)").removeClass("active");  
})


